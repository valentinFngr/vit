import numpy as np 
import torch 



def accuracy_score(y_hat, y_true): 
    return torch.where(y_hat == y_true, 1, 0).to(torch.float32).mean()