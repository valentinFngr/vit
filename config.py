
image_size = 32
patch_size = 4
nb_patches = int((image_size / patch_size)**2)
d_latent = 768
nb_heads = 12
layers = 12
nb_classes = 10

# training
batch_size = 255
device = "cuda"
epochs = 10
dropout_rate = 0.3
beta1 = 0.9 
beta2 = 0.999
learning_rate = 0.001

# dataset 
dataset_folder = "./dataset"