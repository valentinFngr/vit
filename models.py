import torch.nn as nn 
import config
import torch
import torch.nn.functional as F
import matplotlib.pyplot as plt 
import numpy as np

class Embedding(nn.Module): 
    """
        ViT embedding 
    """
    def __init__(self): 
        super(Embedding, self).__init__()
        self.patch_size = config.patch_size
        self.d_latent= config.d_latent
        self.lin_projection = nn.Linear(
            self.patch_size * self.patch_size * 3, 
            self.d_latent, 
            bias=False, 
            device=config.device
        )

        # should be attached to the module
        # LEARNABLE
        self.positional_embedding = nn.parameter.Parameter(
            torch.randn((config.batch_size, 1 + config.nb_patches, self.d_latent), device=config.device)
        )

        # should be attached to the module
        # LEARNABLE
        self.cls_embedding = nn.parameter.Parameter(
            torch.randn((config.batch_size, 1, self.d_latent), device=config.device)
        )

        self.dropout = nn.Dropout(config.dropout_rate)




    def forward(self, x): 
        # TODO : vectorize patch extraction

        # unroll the patch 
        n = x.shape[0]
        patches = []
        for i in range(0, int(np.sqrt(config.nb_patches))): 
            for j in range(0, int(np.sqrt(config.nb_patches))): 
                start_row = i * self.patch_size
                end_row = i * self.patch_size + self.patch_size

                start_col = j * self.patch_size
                end_col = j * self.patch_size + self.patch_size

                patches.append(
                    x[:,:,start_row:end_row, start_col:end_col].reshape(n, -1)
                )

        # patch_embedding : permute nb patch and batch size
        patches = torch.stack(patches)
        patches = patches.permute(1, 0, 2)
        patch_embedding = self.lin_projection(patches)
        patch_embedding = torch.concat([self.cls_embedding, patch_embedding], dim=1)
        input_embedding = patch_embedding + self.positional_embedding
        return self.dropout(input_embedding) # (batch_size, nb_patches, d_latent)


class MultiHeadAttention(nn.Module):

    def __init__(self, out_dim): 
        super(MultiHeadAttention, self).__init__()
        self.out_dim = out_dim
        self.nb_heads = config.nb_heads
        self.d_out = int(out_dim / self.nb_heads)

        # LEARNABLE
        self.key_proj = nn.Linear(config.d_latent, out_dim, bias=False)
        # LEARNABLE
        self.value_proj = nn.Linear(config.d_latent, out_dim, bias=False)
        # LEARNABLE
        self.query_proj = nn.Linear(config.d_latent, out_dim, bias=False)
        # LEARNABLE
        self.lin_proj = nn.Linear(config.d_latent, config.d_latent)

        self.dropout = nn.Dropout(config.dropout_rate)

    def _heads_reshape_multihead(self, x): 
        """
            reshape the input 
            Argument: 
                x : (batch_size, nb_patches, d_latent)
            Output: 
                res : (batch_size, nb_heads, nb_patches, d_latent)
            
        """

        x  = torch.reshape(x, shape=((x.shape[0], x.shape[1],  self.nb_heads, self.d_out)))
        # permutation 
        res = x.permute(0, 2, 1, 3)
        return res

    def forward(self, x): 
        """
            Argument: 
                x : (batch_size, nb_patches, d_latent)
            Output: 
                res : (batch_size, nb_patches, d_latent)
        """

        K = self._heads_reshape_multihead(self.key_proj(x))  # (batch_size, nb_heads, nb_patches, d_latent)
        Q = self._heads_reshape_multihead(self.query_proj(x))  # (batch_size, nb_heads, nb_patches, d_latent)
        V = self._heads_reshape_multihead(self.query_proj(x))  # (batch_size, nb_heads, nb_patches, d_latent)

        # note that matmul cares only about the 2 last dims 
        attn_score = torch.matmul(Q, K.transpose(-1, -2))  # (batch_size, nb_heads, nb_patches, nb_patches)
        softmax_attn = F.softmax(attn_score / torch.sqrt(torch.tensor(config.d_latent)), dim=-1)
        attn_value = torch.matmul(softmax_attn, V) # (batch_size, nb_heads, nb_patches, d_latent)
        attn_value = attn_value.permute(0, 2, 1, 3)
        attn_value = attn_value.reshape(config.batch_size, config.nb_patches + 1, -1)
        res = self.lin_proj(attn_value)
        res = self.dropout(res)
        return res

        


class MLP(nn.Module): 
    """
        ViT feed forward (mlp)
    """

    def __init__(self, latent_dim): 
        super(MLP, self).__init__()
        # LEARNABLE
        self.linear1 = nn.Linear(config.d_latent, latent_dim)
        # LEARNABLE
        self.linear2 = nn.Linear(latent_dim, config.d_latent)

        self.dropout = nn.Dropout(config.dropout_rate)
        self.gelu = nn.GELU() 

    def forward(self, x): 
        """
            Arguments: 
                x : (batch_size, nb_patches, in_dim)

            Output: 
                res : (batch_size, nb_patches, out_dim)
        """
        x = self.dropout(self.gelu(self.linear1(x)))
        res = self.dropout(self.linear2(x))
        return res


class EncoderBlock(nn.Module):
    def __init__(self): 
        super(EncoderBlock, self).__init__()
        self.multihead_attn = MultiHeadAttention(config.d_latent)
        self.mlp = MLP(300)
        # LEARNABLE
        self.layerNorm1 = torch.nn.LayerNorm(config.d_latent)
        # LEARNABLE
        self.layerNorm2 = torch.nn.LayerNorm(config.d_latent)

    def forward(self, x): 
        """
            Arguments: 
                x : (batch_size, nb_patches, d_latent)

            Output: 
                res : (batch_size, nb_patches, d_latent)
        """

        residual = x 
        x = self.layerNorm1(x)
        x = self.multihead_attn(x) + residual
        residual = x 
        x = self.layerNorm2(x)
        x = self.mlp(x) + residual
        return x 


class Encoder(nn.Module): 

    def __init__(self):
        super(Encoder, self).__init__() 
        blocks = [
            EncoderBlock() for i in range(config.layers)
        ]
        self.blocks = nn.Sequential(*blocks)
    def forward(self, x): 
        return self.blocks(x)

        
class VisionTransformer(nn.Module): 

    def __init__(self): 
        super(VisionTransformer, self).__init__()
        self.nb_classes = config.nb_classes
        self.encoder = Encoder()
        self.create_embedding = Embedding()
        # LEARNABLE
        self.clf_mlp = nn.Linear(config.d_latent, config.nb_classes)

    def forward(self, x): 
        """ 
            Argument: 
                x : (batch_size, 3, H, W) image
            Output: 
                logit : (batch_size, nb_classes)
        """
        embeddings = self.create_embedding(x)
        # feed to encoder
        encoder_output = self.encoder(embeddings)
        cls_token_encoder = encoder_output[:, 0]
        logit = self.clf_mlp(cls_token_encoder)
        return logit


