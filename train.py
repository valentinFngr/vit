import torch.nn as nn 
import torch
from torch.utils.data import random_split,DataLoader 
import torchvision.transforms as transforms
import numpy as np 
from torchvision.datasets import CIFAR10
import config
from torch.optim import Adam
from models import VisionTransformer
from torchsummary import summary
from torch.utils.tensorboard import SummaryWriter
import matplotlib.pyplot as plt

from utils import accuracy_score

def get_loss(): 
    """
        return the loss function (softmax cross entropy)
    """

    ce = torch.nn.CrossEntropyLoss().to(config.device)
    return ce


def load_model(): 
    vit = VisionTransformer().to(config.device)
    return vit 
    

def load_optimizers(model): 
    optimizer = Adam(model.parameters(), lr=config.learning_rate, betas=(config.beta1, config.beta2))
    return optimizer



def get_cifar101(): 
    """
        Load cifar10
    """
    train = CIFAR10(
        config.dataset_folder, 
        download=True,
        train=True,
        transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
    )
    # train, test = random_split(dataset, (len()))
    test = CIFAR10(
        config.dataset_folder, 
        download=True,
        train=False,
        transform=transforms.Compose([
            transforms.ToPILImage(),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
    )

    train_loader = DataLoader(
        train, 
        batch_size=config.batch_size,
        shuffle=True
    )

    test_loader = DataLoader(
        test, 
        batch_size=config.batch_size,
        shuffle=True
    )

    return train_loader, test_loader


def train(train_loader, model, optimizer, ce_criterion, writer, epoch): 
    total_loss = 0.0
    model.train()
    for i, (images, labels) in enumerate(train_loader): 
        
        images = images.to(config.device)
        labels = labels.to(config.device)
                
        y_hat = model(images)            
        # compute loss 
        loss = ce_criterion(y_hat, labels)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

        # monitor 

        with torch.no_grad():  
            total_loss += loss.item()
            # print("preds : ", preds)
            # print("labels ", labels)
            print(loss.item())

            if i % 20 == 0: 
                preds = nn.Softmax(dim=1)(y_hat)
                print(preds)
                accuracy = accuracy_score(preds.argmax(1), labels)
                writer.add_scalar("train/loss", total_loss / 20.0   , epoch*len(train_loader) + i + 1)
                total_loss = 0.0
            # print("accuracy : ", accuracy)


def main():
    
    print("--- load dataset ---")
    train_loader, test_loader = get_cifar101()
    print("--- load dataset : DONE ---", "\n")

    print("--- load dataset ---")
    ce_criterion = get_loss()
    print("--- load dataset : DONE ---", "\n")

    print("--- load vision transformer ---")
    model = load_model()
    print("--- load vision transformer : DONE---", "\n")

    print("--- load optimizer ---")
    optimizer = load_optimizers(model)
    print("--- load optimizer : DONE ---", "\n")

    print("--- load writer ---")
    writer = SummaryWriter()
    print("--- load writer : DONE ---", "\n")

    print("--- Training ---")
    for epoch in range(config.epochs): 
        print(f"epoch / {config.epochs} : ", epoch)
        train(train_loader, model, optimizer, ce_criterion, writer, epoch)
        
    print("--- Training : DONE ---", "\n")



if __name__ == "__main__": 
    main()
